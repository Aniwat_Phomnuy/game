using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class score : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;

    private void Awake()
    {
        Debug.Assert(scoreText != null, "scoreText cannot null");
        
        ShowScore(true);
    }
    
    public void ShowScore(bool showScore)
    {
        //UpdateScoreUI();
        scoreText.gameObject.SetActive(showScore);
    }

}
